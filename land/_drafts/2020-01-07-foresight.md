---
layout: page
title: "2020 (what hindsight never is)"
categories: land
tags: [radio, amateur, ham, license]
---

Well, it's a new year, and for one of my NY resolutions I've decided to write a blog post once per month. So here goes January!

One saying I disagree with is the notion that "hindsight is always 2020". It usually is not. And after all, only a Sith deals in absolutes. Nevertheless, I was highly disappointed with the lack of perfect vision puns over the holiday from my family and the twittersphere.

We could get philisophical, abstract, or political here, but since I'd like this blog to be technical, I think some technical updates are more worth their due.

For starters, I got my Bachelors and got employed, which is pretty cool so far.

I purchased a couple domains, and will be spreading my site out. whatit.is will be an archive of my radio shows from WRUW, and any new broadcast content will be hosted on that site. bowcutt.dev, my personal homepage and hopefully more space to tinker.

I would like to work on some radio comm projects, since getting a license last March and gaining interest during my senior project. Unfortunately, I live in an apartment complex with metal balcony railings, which makes it difficult to get a good transmission in. I have an antenna that sits at my window, plugged into a Baofeng UV-5R - so I guess I'm in need of some upgrades. I'm interested in digital modes.

I built a laptop gaming rig using an Thunderbolt GPU. It's good when it works, but maybe 40% of boots end up crashing the entire system 3-5 minutes into gameplay. Additionally, when the system sleeps the enclosure sleeps too, but it won't wake when the system starts up again. So, I'm getting pretty used to rebooting and I'm using Win10 pretty regularly, but thank goodness for fastboot.


Learning Notes:

1/5/2020
PCIE, I2C buses

1/6/2020
Finished I2C and started SPI bus

1/7/2020
Learned about filesystems - FAT**, NTFS, exFAT, ext*, HFS/HFS+/APFS, ZFS