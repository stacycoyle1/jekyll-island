---
layout: page
title: bit farm
permalink: /land/bit-farm/
---

The code for my open-source projects are hosted around at [GitLab](https://gitlab.com/mbowcutt).

## Personal Projects

### [jekyll.is/land](https://gitlab.com/mbowcutt/jekyll-island)

This is this website! Built using [Jekyll](https://jekyllrb.com).

### [Mimic](https://gitlab.com/mbowcutt/mimic)

Mimic is a [flask](http://flask.pocoo.org/) powered full-stack web application for multi-persona text generation. It uses the [markovify](https://github.com/jsvine/markovify) package to learn against texts, uploaded via Github Gists, and saves a JSON model representing a persona to a SQLite database. A clunky frontend allows the user to create personas and generate text segments for each.

This project was inspired by the work of my professor, [Timothy Beal](http://www.timothybeal.com/) who's experimented with creative text generation using sources like the works of Walt Whitman, Emily Dickinson, and the KJV Bible.

## School Projects

### [Embedded Systems Labs](https://gitlab.com/mbowcutt/303-labs)

My embedded systems coursework involved various assignments involving creating simple circuits and writing mostly C (some Python) for Raspberry Pi and Arduino platforms.

### [Digital Logic Labs](https://gitlab.com/mbowcutt/301-labs)

My digital logic laboratory involved writing Verilog code for an FPGA using the Intel Quartus Prime design software.

### [Computer Networking Projects](https://gitlab.com/mbowcutt/325-projects)

My computer networking projects included developing a barebones web client, web server, and packet analysis tool in C.

The client and server are awfully dumbed down, as they are mainly implementations of the HTTP spec. For example, the client only downloads files, like curl. The server only responds to GET requests and introduces a QUIT request to implement a remote "secure" shutdown.

The packet analysis tool digs through a given packet trace (with custom frame headers) and reports data on the ethernet, IP, and TCP/UDP layers in one of four ways.

### [FF4TV](https://gitlab.com/mbowcutt/ff4tv)

Fantasy Football 4 TV was a five-person project for a software engineering course. It's a full stack web-app that allows users to create leagues of teams, invite friends, and select rosters of television shows, that are scored with IMDb ratings. Written in Node and powered with Express.js.

### [Term-Com](https://gitlab.com/mbowcutt/term-com)

My final project for an operating systems and concurrent programming course is an console chat room written in Go.
