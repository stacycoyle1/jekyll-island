---
layout: post
title: "Site Updates"
category: land
tags: [meta, updates, jekyll, ruby, haunt, guile, emacs, lisp, org-mode]

---

I've made a round trip. After experimenting with a site engines, notably [Haunt](https://dthompson.us/projects/haunt.html), and hacking some configurations in Emacs Lisp with [org-mode](https://orgmode.org), I'm back to using [Jekyll](https://jekyllrb.com). 

I must admit, I'm very attracted to the idea of using a Lisp-based program, and as I just opened the page for Haunt I was itching to try it out again. Maybe I will in a little while from now, but now that I've found I have some technical things to blog about, I've given hope on creating _my own_ thing and will use something that already works.

So here I am, using Jekyll. Let's get this ball rolling!
