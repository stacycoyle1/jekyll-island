---
layout: page
title: about
permalink: /land/about/
---

Greetings, I'm Michael. I'm a sysadmin, netadmin, textbook geek, music addict, radio head, packet pioneer and console cowboy. This is my blog. Welcome to Jekyll Island!

This site is built using the static site generator [jekyll](https://jekyllrb.com).
